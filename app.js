require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const config = require('config');

const tweetRouter = require('./routes/tweet');
const changeRequestRouter = require('./routes/changeRequest');
const superAdminRouter = require('./routes/superAdmin');
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/tweet', tweetRouter);
app.use('/changeRequest', changeRequestRouter);
app.use('/superAdmin', superAdminRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

app.use((err, req, res, next) => {
  return res.status(err.status).send({ "errorMessage": err.message });
});

//For ease of setup for code reviewer, taking db url and credentials from config file instead of environment variables
mongoose.connect(config.db.url)
    .then(() => console.log("Connected to Db"))
    .catch(err => console.log(`Error connecting ${err}`));

module.exports = app;
