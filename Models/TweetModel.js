const mongoose = require('mongoose');

let Tweet = new mongoose.Schema({
    text: {type: String},
    imageUrl: {type: String},
    isDeleted: {type: Boolean, default: false},
    createdAt: {type: Date, default: new Date()},
    updatedAt: {type: Date, default: new Date()},
    user : {
        id: {type: mongoose.Schema.Types.ObjectId, ref: "User"},
        name: {type: String}
    }
});

module.exports = mongoose.model('Tweet', Tweet);
