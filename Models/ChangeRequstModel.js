const mongoose = require('mongoose');

let ChangeRequest = new mongoose.Schema({
    requestedBy: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    type: {type: String, required: true},
    user: {
        id: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        name: {type: String}
    },
    tweet: {
        id: {type: mongoose.Schema.Types.ObjectId, ref: 'Tweet'},
        text: {type: String},
        imageUrl: {type: String},
        isDeleted: {type: Boolean},
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    },
    isApproved: {type: Boolean, default: false}
});

module.exports = mongoose.model('ChangeRequest', ChangeRequest);
