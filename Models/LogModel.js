const mongoose = require('mongoose');

let Log = new mongoose.Schema({
    accessLogs : [{
        log: {type: String},
        timestamp: {type: Date, default: Date.now()}
    }],
    actionLogs : [{
        log: {type: String},
        timestamp: {type: Date, default: Date.now()}
    }],
    auditLogs : [{
        log: {type: String},
        timestamp: {type: Date, default: Date.now()}
    }],
});

module.exports = mongoose.model('Log', Log);
