const LogModel = require('../Models/LogModel');
async function saveLog(source, action, object) {
    let log = await LogModel.findById("5ff9940e29310764dd1f32ed");
    if(source === "user") {
        let str = action === "created" ? "tweets" : "deleted" ;
        let logStr = `User ${object.user.id} ${str} ${object._id}`;
        if(action === "created") {
            log.actionLogs.push({
                log: logStr
            });
        } else {
            log.auditLogs.push({
                log: logStr
            });
        }
    } else if(source === "admin") {
        let str = object.type === "tweet" ? "tweet " + object.tweet.id : "user " + object.user.id;
        let logStr = `Admin ${object.requestedBy} requested an update for ${str}`;
        log.actionLogs.push({
            log: logStr
        });
    } else if(source === "superadmin") {
        let logStr = `SuperAdmin approved changerequest ${object._id}`;
        log.auditLogs.push({
            log: logStr
        });
    }
    log.save();
}

module.exports = {
  saveLog: saveLog
};

