const express = require('express');
const router = express.Router();
const httpError = require('http-errors');
const User = require("../Models/UserModel");
const Tweet = require("../Models/TweetModel");
const loggingService = require('../services/loggingService');

router.post('/', async function(req, res, next) {
  let userId = req.body.userId;
  let user = await User.findById(userId);
  if(!user) {
    return next(httpError(404, 'User Not Found'));
  }
  let tweet = new Tweet({
    text: req.body.text,
    user : {
      id: user._id,
      name: user.name
    }
  });
  let result = await tweet.save();
  loggingService.saveLog("user", "created", tweet);
  res.send(result);
});

router.get('/getList/:userId', async function (req, res, next) {
  let userId = req.params.userId;
  let user = await User.findById(userId);
  if(!user) {
    return next(httpError(404, 'User Not Found'));
  }
  let tweets = await Tweet.find({
    'user.id': userId,
     isDeleted: false
  }).sort({createdAt: -1});
  res.send(tweets);
});

router.put('/delete', async function (req, res, next) {
  let userId = req.body.userId;
  let tweetId = req.body.tweetId;
  let user = await User.findById(userId);
  let tweet = await Tweet.findById(tweetId);
  if(!user || !tweet) {
    return next(httpError(404, 'Data Invalid'));
  }
  tweet.isDeleted = true;
  const result = await tweet.save();
  loggingService.saveLog("user", "deleted", tweet);
  res.send(result);
});

module.exports = router;
