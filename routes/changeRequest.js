const express = require('express');
const router = express.Router();
const httpError = require('http-errors');
const User = require("../Models/UserModel");
const ChangeRequest = require("../Models/ChangeRequstModel");
const loggingService = require("../services/loggingService");

router.post('/', async function (req, res, next) {
    let userId = req.body.userId;
    let user = await User.findById(userId);
    if(!user) {
        return next(httpError(404, 'User Not Found'));
    }
    if(user.type !== "admin") {
        return next(httpError(401, 'User Unauthorized'));
    }
    let changeType = req.body.changeType;
    if(!(changeType === "user" || changeType === "tweet")) {
        return next(httpError(400, 'Bad Data'));
    }
    let changeRequest;
    if(changeType === "user") {
        changeRequest = new ChangeRequest({
           requestedBy: userId,
           type: changeType,
           user : {
               id: req.body.userToChangeId,
               name: req.body.name
           }
        });
    } else if(changeType === "tweet") {
        changeRequest = new ChangeRequest({
            requestedBy: userId,
            type: changeType,
            tweet: {
                id: req.body.tweetId,
                userId: req.body.behalfUserId,
                text: req.body.text,
                imageId: req.body.imageId,
                isDeleted: req.body.isDeleted
            }
        });
    }
    let result = await changeRequest.save();
    loggingService.saveLog("admin", changeType, changeRequest);
    res.send(result);
});
module.exports = router;
