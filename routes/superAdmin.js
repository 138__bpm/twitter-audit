const express = require('express');
const router = express.Router();
const User = require("../Models/UserModel");
const Tweet = require("../Models/TweetModel");
const ChangeRequest = require("../Models/ChangeRequstModel");
const httpError = require('http-errors');
const loggingService = require('../services/loggingService');
router.post('/approve', async function (req, res, next) {
    let userId = req.body.userId;
    let user = await User.findById(userId);
    if(!user) {
        return next(httpError(404, 'User Not Found'));
    }
    if(user.type !== "superadmin") {
        return next(httpError(401, 'User Unauthorized'));
    }
    let changeRequestId = req.body.changeRequestId;
    let changeRequest = await ChangeRequest.findById(changeRequestId);
    if(!changeRequest) {
        return next(httpError(404, 'Change Request Not Found'));
    }
    let changeType = changeRequest.type;
    if(changeType === "user") {
        let userToModifyId = changeRequest.user.id;
        await User.findByIdAndUpdate(userToModifyId, {
            "name": changeRequest.user.name
        });
    } else if(changeType === "tweet"){
        let tweetId = changeRequest.tweet.id;
        if(tweetId) {
            let tweet = await Tweet.findById(tweetId);
            tweet.set({
                "text": changeRequest.tweet.text || tweet.text,
                "imageUrl": changeRequest.tweet.imageUrl || tweet.imageUrl,
                "deleted": changeRequest.tweet.deleted || tweet.deleted,
                "updatedAt": new Date()
            });
            await tweet.save();
        } else {
            let behalfUser = await User.findById(changeRequest.tweet.userId);
            let tweet = new Tweet({
                text: changeRequest.tweet.text,
                user : {
                    id: behalfUser._id,
                    name: behalfUser.name
                }
            });
            await tweet.save();
        }
    }
    changeRequest.isApproved = true;
    let result = await changeRequest.save();
    loggingService.saveLog("superadmin", null, changeRequest);
    res.send(result);
});

router.get('/insights/changesRequested', async function (req, res, next) {
    let userId = req.header('userId');
    let adminId = req.header('adminId');
    let user = await User.findById(userId);
    let admin = await User.findById(adminId);
    if(!user || !admin) {
        return next(httpError(404, 'User Not Found'));
    }
    if(user.type !== "superadmin") {
        return next(httpError(401, 'User Unauthorized'));
    }

    let changeRequests = await ChangeRequest.find({
        requestedBy: adminId
    });
    res.send({
        count: Object.keys(changeRequests).length
    });
});

module.exports = router;
